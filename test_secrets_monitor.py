import textwrap

from utilities import types

test_url = "https://www.test.com/api/v1/test"


def test_handles_empty_string():
    target = types.SecretsMonitor()
    content = {test_url: ""}
    assert target.sniff_secrets(content) == []


def test_handles_nil():
    target = types.SecretsMonitor()
    content = {test_url: None}
    assert target.sniff_secrets(content) == []


def test_finds_simple_json_gitlab_pat():
    target = types.SecretsMonitor()

    content = {test_url: "private-token:glpat-XYA_12345-aaaaaaaaaa\r\n"}
    actual = target.sniff_secrets(content)
    assert len(actual) == 1
    assert actual[0].url == test_url
    assert actual[0].secret == "glpat-XYA_12345-aaaaaaaaaa"
    assert actual[0].secret_type == "GitLab PAT"


def test_regexes_are_loaded():
    target = types.SecretsMonitor()
    assert len(target.regexes) > 0


def test_does_not_match_gitlab_pats_without_line_ending():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""
        [GitLab Personal Access Token Example](https://gitlab.com/gitlab-com/gl-security/gl-redteam/red-team-tech-notes/snippets/1976052)
    """)}
    actual = target.sniff_secrets(content)
    assert len(actual) == 0


def test_finds_gitlab_pat_in_naked_string():
    target = types.SecretsMonitor()
    content = {test_url: "private-token: glpat-QMZd94_z-MAVNfaoP7Vz"}
    actual = target.sniff_secrets(content)
    assert len(actual) == 1


def test_finds_gitlab_pat_surrounded_by_double_quotes():
    target = types.SecretsMonitor()
    content = {test_url: "PRIVATE_TOKEN= \"glpat-QMZd94_z-MAVNfaoP7Vz\""}
    actual = target.sniff_secrets(content)
    assert len(actual) == 1


def test_finds_gitlab_pat_in_text_block():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
                using System.Collections.Generic;
                using System.Runtime.CompilerServices;

                namespace NameSpace1
                {
                    public static class DoubleExecutionPreventerExtensions
                    {
                        private static readonly List<string> locks = new List<string>();

                        public static void Free(this object obj, [CallerMemberName] string caller = null)
                        {
                            string key = GetKey(obj, caller);
                            locks.Remove(key);
                        }

                        public static bool Lock(this object obj, [CallerMemberName] string caller = null)
                        {
                            string key = GetKey(obj, caller);

                            if (locks.Contains(key))
                                return true;

                            locks.Add(key);
                            return false;
                        }

                        private static string GetKey(object instance, string caller)
                        {
                            return "private-token=glpat-QMZd94SzIMAVNfaoP7Vz"
                        }
                    }
                }
            """)}
    actual = target.sniff_secrets(content)
    assert len(actual) == 1
    assert actual[0].secret == 'glpat-QMZd94SzIMAVNfaoP7Vz'
    assert actual[0].url == test_url
    assert actual[0].secret_type == "GitLab PAT"


def test_finds_naked_slack_token():
    target = types.SecretsMonitor()
    naked_token = "xoxb-931357323073-954664552758-1IrxU4wByo3exZviLEMibCTw"
    content = {test_url: naked_token}
    actual = target.sniff_secrets(content)
    assert len(actual) == 1
    assert actual[0].url == test_url
    assert actual[0].secret == "xoxb-931357323073"
    assert actual[0].secret_type == "Slack Token"


def test_finds_single_group_results():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
            -----BEGIN RSA PRIVATE KEY-----
            asdfjwpoidnsohfoHoiahsdfkjaksfdkasdfsdkfjlhkjhslkdjhdfjh
            -----END RSA PRIVATE KEY-----
        """)}
    assert len(target.sniff_secrets(content)) == 1


def test_finds_openssh_private_key():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
                    -----BEGIN OPENSSH PRIVATE KEY-----
                    asdfjwpoidnsohfoHoiahsdfkjaksfdkasdfsdkfjlhkjhslkdjhdfjh
                    -----END OPENSSH PRIVATE KEY-----"
                """)}
    assert len(target.sniff_secrets(content)) == 1


def test_finds_gitlab_ci_registration_token():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
    runners:
    - name: ***computer name***
      limit: 0
      outputlimit: 0
      requestconcurrency: 0
      runnercredentials:
        url: https://gitlab.com/
        token: guz_DJCzb4rsUybpwuAQ
        tlscafile: ""
        tlscertfile: ""
        tlskeyfile: ""
      runnersettings:
        executor: docker
        buildsdir: ""
        cachedir: ""
    """)}
    assert len(target.sniff_secrets(content)) == 1

def test_ignores_hashes1():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
    [INFO] SCM collecting changed files in the branch
    [INFO] Merge base sha1: 6a0ce2dac15f0151652060d0d4ab41568022f3f2
    [INFO] SCM collecting changed files in the branch (done) | time=404ms
    """)}
    assert len(target.sniff_secrets(content)) == 0

def test_ignores_hashes2():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
    [INFO] SCM collecting changed files in the branch
    [INFO] commit 6a0ce2dac15f0151652060d0d4ab41568022f3f2
    [INFO] SCM collecting changed files in the branch (done) | time=404ms
    """)}
    assert len(target.sniff_secrets(content)) == 0

def test_ignores_hashes3():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
    [INFO] SCM collecting changed files in the branch
    [ERROR] fatal: unable to read tree 6a0ce2dac15f0151652060d0d4ab41568022f3f2
    [INFO] SCM collecting changed files in the branch (done) | time=404ms
    """)}
    assert len(target.sniff_secrets(content)) == 0

def test_ignores_hashes4():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
    Init Binary: docker-init
    containerd version: 269548fa27e0089a8b8278fc4fc781d7f65a939b
    runc version: ff819c7e9184c13b7c2607fe6c30ae19403a7aff
    init version: de40ad0
    """)}
    assert len(target.sniff_secrets(content)) == 0

def test_ignore_list1():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
    remote: You are not allowed to download code from this project.
    fatal: unable to access 'https://gitlab-ci-token:[MASKED]@rc-vmgitlab.myserver.com/ams/Frontend.git/': The requested URL returned error: 403
    ERROR: Job failed: exit code 1
    """)}
    assert len(target.sniff_secrets(content)) == 0

def test_ignore_list2():
    target = types.SecretsMonitor()
    content = {test_url: textwrap.dedent("""\
    env.CI_BUILD_REPO=https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@RC-VMGITLAB.myserver.com/ams/Backend.git, os.arch=amd64, 
    """)}
    assert len(target.sniff_secrets(content)) == 0