import requests

from api import http


def test_adjust_paging_with_no_params():
    target = http.Http(lambda: requests.Session())
    url = "https://gitlab.com/api/v4/projects/14171783/jobs"
    expected = "https://gitlab.com/api/v4/projects/14171783/jobs?per_page=20"
    actual = target.__adjust_paging__(url, 20)
    assert expected == actual


def test_adjust_paging_with_existing_params():
    target = http.Http(lambda: requests.Session())
    url = "https://gitlab.com/api/v4/projects/14171783/jobs?scope=success&scope=failed"
    expected = "https://gitlab.com/api/v4/projects/14171783/jobs?scope=success&scope=failed&per_page=20"
    actual = target.__adjust_paging__(url, 20)
    assert expected == actual


def test_adjust_paging_with_existing_per_page_query_param():
    target = http.Http(lambda: requests.Session())
    url = "https://gitlab.com/api/v4/projects/14171783/jobs?scope=success&scope=failed&per_page=20"
    expected = "https://gitlab.com/api/v4/projects/14171783/jobs?scope=success&scope=failed&per_page=10"
    actual = target.__adjust_paging__(url, 10)
    assert expected == actual
